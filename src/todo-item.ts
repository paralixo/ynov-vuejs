export default interface ITodoItem {
    title: string;
    hours: number;
    assignee: any;
    done: boolean;
    selected: boolean;
}